import {Entity, model, property} from '@loopback/repository';

@model()
export class Comment extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    default: "Visitor",
  })
  Username?: string;

  @property({
    type: 'number',
    default: 0,
  })
  Likes?: number;

  @property({
    type: 'number',
    default: 0,
  })
  Dislikes?: number;

  @property({
    type: 'string',
  })
  Answerto?: string;

  @property({
    type: 'string',
    required: true,
  })
  Description: string;


  constructor(data?: Partial<Comment>) {
    super(data);
  }
}

export interface CommentRelations {
  // describe navigational properties here
}

export type CommentWithRelations = Comment & CommentRelations;
