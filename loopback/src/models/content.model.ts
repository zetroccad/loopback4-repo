import {Entity, model, property} from '@loopback/repository';

@model()
export class Content extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  Filename: string;

  @property({
    type: 'string',
  })
  Description?: string;

  @property({
    type: 'string',
    required: true,
  })
  Type: string;


  constructor(data?: Partial<Content>) {
    super(data);
  }
}

export interface ContentRelations {
  // describe navigational properties here
}

export type ContentWithRelations = Content & ContentRelations;
