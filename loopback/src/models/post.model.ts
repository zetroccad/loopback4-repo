import {Entity, model, property} from '@loopback/repository';

@model()
export class Post extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  ID?: number;

  @property({
    type: 'string',
    required: true,
  })
  Title: string;

  @property({
    type: 'string',
  })
  Description?: string;

  @property({
    type: 'date',
    required: true,
  })
  Date: string;

  @property({
    type: 'string',
  })
  Location?: string;

  @property({
    type: 'number',
    default: 0,
  })
  Likes?: number;

  @property({
    type: 'number',
    default: 0,
  })
  Dislikes?: number;


  constructor(data?: Partial<Post>) {
    super(data);
  }
}

export interface PostRelations {
  // describe navigational properties here
}

export type PostWithRelations = Post & PostRelations;
